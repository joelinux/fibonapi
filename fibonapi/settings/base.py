"""Base config for this project."""
APP_NAME = "FibonAPI"
DEBUG = True

# the default number of Fibonacci numbers to return if none specified
FIBONACCI_MAX = 200
