"""URLs to match for request handlers."""
from fibonapi import handlers


URLS = (
    (r'^/fibonacci/?', handlers.FibonacciHandler),
)
