"""Run main tornado application."""
# stdlib imports
import logging

# 3rd party imports
import tornado.web

# local imports
from fibonapi import settings


class FibApplication(tornado.web.Application):
    """Wrapper for Tornado Application."""

    def __init__(self, handlers, **kwargs):
        """Set up handlers and settings.

        Args:
            handlers (list): a list of tuples containing URL rules and handlers
        """
        level = logging.DEBUG if kwargs.get('debug') else logging.INFO
        logging.basicConfig(level=level, handlers=[logging.StreamHandler()])
        super().__init__(handlers, **kwargs)
        self.log_level = level
        self.config = settings


def run_app(app):
    """Run a Tornado application.

    Args:
        app (tornado.web.Application): the Tornado app instance to run
    """
    import tornado.httpserver
    from tornado.options import define, options

    # CLI options
    define('port', default=8080, help="Port to listen on")
    tornado.options.parse_command_line()  # this also set up pretty logging

    logger = logging.getLogger(__name__)
    logger.setLevel(app.log_level)
    logger.info("Listening on port %s", options.port)
    try:
        if not app.config.DEBUG:
            server = tornado.httpserver.HTTPServer(app)
            server.bind(options.port)
            server.start(0)  # auto-detect cores and fork
        else:
            # for DEBUG apps, autoreload is turned on, and won't work with
            # multiple processes
            logger.info("Debug mode enabled")
            app.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    from fibonapi.urls import URLS

    # initialize and start application
    app = FibApplication(handlers=URLS, debug=settings.DEBUG)
    run_app(app)
