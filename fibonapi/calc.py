"""Methods that are logically separate from serving web requests."""
from tornado import gen


@gen.coroutine
def fibonacci(n):
    """Return `n` Fibonacci numbers."""
    nums = [0, 1]
    if n < 2:
        return nums[:n]
    [nums.append(nums[-1] + nums[-2]) for i in range(2, n)]
    return nums
