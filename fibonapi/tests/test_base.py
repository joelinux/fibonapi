"""Test request handler base methods."""
import tornado.ioloop
import tornado.testing

from fibonapi.app import FibApplication
from fibonapi.urls import URLS


class BaseTest(tornado.testing.AsyncHTTPTestCase):
    """Test HandlerMixin and CORS."""

    def get_app(self):
        """Return an instance of the Application object."""
        self.application = FibApplication(handlers=URLS, debug=True)
        return self.application

    def get_new_ioloop(self):
        """Prevent the 'tornado.autoreload started more than once' warning."""
        return tornado.ioloop.IOLoop.current()

    def test_allow_origin(self):
        """Should allow hosts in the ALLOWED_HOSTS config."""
        host = self.application.config.ALLOWED_HOSTS[0]
        response = self.fetch('/fibonacci/?n=1', headers={'Origin': host})
        self.assertEqual(response.code, 200)

    def test_preflight(self):
        """Should always respond successfully to preflight requests."""
        response = self.fetch('/fibonacci/?n=1', method='OPTIONS')
        self.assertEqual(response.code, 204)
