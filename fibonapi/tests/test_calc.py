"""Test the calc module."""
from tornado.testing import AsyncTestCase, gen_test

from fibonapi.calc import fibonacci


class CalcTest(AsyncTestCase):
    """Class for Fibonacci tests."""

    @gen_test
    def test_empty_fibonacci(self):
        """Should return no Fibonacci numbers when arg is zero."""
        nums = yield fibonacci(0)
        self.assertListEqual(nums, [])

    @gen_test
    def test_first_three_fibonacci(self):
        """Should return first three Fibonacci numbers correctly."""
        nums = yield fibonacci(1)
        self.assertListEqual(nums, [0])

        nums = yield fibonacci(2)
        self.assertListEqual(nums, [0, 1])

        nums = yield fibonacci(3)
        self.assertListEqual(nums, [0, 1, 1])

    @gen_test
    def test_fibonacci(self):
        """Should calculate Fibonacci numbers correctly."""
        nums = yield fibonacci(7)
        self.assertEqual(nums[-1], 8)
        self.assertEqual(len(nums), 7)

        nums = yield fibonacci(10)
        self.assertEqual(nums[-1], 34)
        self.assertEqual(len(nums), 10)

        nums = yield fibonacci(41)
        self.assertEqual(nums[-1], 102334155)
        self.assertEqual(len(nums), 41)
