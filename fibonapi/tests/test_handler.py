"""Test the request handlers."""
import tornado.ioloop
import tornado.testing
from tornado.escape import json_decode

from fibonapi import settings
from fibonapi.app import FibApplication
from fibonapi.urls import URLS


class FibonacciTest(tornado.testing.AsyncHTTPTestCase):
    """Test the FibonacciHandler."""

    def get_app(self):
        """Return an instance of the Application object."""
        self.application = FibApplication(handlers=URLS, debug=True)
        return self.application

    def get_new_ioloop(self):
        """Prevent the 'tornado.autoreload started more than once' warning."""
        return tornado.ioloop.IOLoop.current()

    def test_valid_fibonacci(self):
        """Should return the appropriate number of Fibonacci numbers."""
        n = 7
        response = self.fetch('/fibonacci/?n=%d' % n)
        values = json_decode(response.body)['values']
        self.assertEqual(response.code, 200)
        self.assertEqual(len(values), n)
        self.assertEqual(values[-1], 8)

    def test_negative_fibonacci(self):
        """Should correctly fail on negative Fibonacci numbers."""
        n = -1
        response = self.fetch('/fibonacci/?n=%d' % n)
        self.assertEqual(response.code, 400)

    def test_default_fibonacci(self):
        """Should return default amount of Fibonacci numbers without input."""
        response = self.fetch('/fibonacci/')
        values = json_decode(response.body)['values']
        self.assertEqual(response.code, 200)
        self.assertEqual(len(values), settings.FIBONACCI_MAX)
        self.assertEqual(values[-1], 173402521172797813159685037284371942044301)

    def test_no_fibonacci(self):
        """Should return not found for an invalid URL."""
        response = self.fetch('/')
        self.assertEqual(response.code, 404)

    def test_invalid_fibonacci(self):
        """Should correctly fail on non-integer input."""
        n = 'foo'
        response = self.fetch('/fibonacci/?n=%s' % n)
        self.assertEqual(response.code, 400)
