"""Handler for listening for Fibonacci requests."""
# stdlib imports
import logging

# 3rd party imports
from tornado import gen

# local imports
from fibonapi.calc import fibonacci
from .base import BaseHandler, log_sandwich


class FibonacciHandler(BaseHandler):
    """Handle Fibonacci number sequences."""

    @log_sandwich
    @gen.coroutine
    def get(self):
        """Return a sequence of Fibonacci numbers."""
        logger = logging.getLogger(__name__)
        logger.setLevel(self._log_level)
        fib_max = self.get_argument('n', self._config.FIBONACCI_MAX)
        try:
            fib_max = int(fib_max)
        except ValueError:
            self.write_error(400, "'%s' is not an integer" % repr(fib_max))
            return

        # ensure a valid number
        if fib_max < 0:
            self.write_error(400, "Provided number must be greater than zero")
            return

        # calculate number sequence
        logger.debug("Calculating %d Fibonacci numbers", fib_max)
        fibs = yield gen.Task(fibonacci, fib_max)

        self.write({'values': fibs, 'type': 'fibonacci'})
