"""Base Tornado request handler classes and utilities."""
# stdlib imports
import logging
from urllib.parse import urlparse

# 3rd party imports
import tornado.web


class HandlerMixin(object):
    """Mixin for common utilities."""

    def _is_allowed_origin(self, request, allowed_hosts):
        """Verify whether `request` comes from an Origin within `allowed_hosts`.

        Args:
            request (HTTPRequest): a Tornado request object
            allowed_hosts (iter): domains that are allowed access

        Returns:
            (bool) True if Origin in `request` is allowed access
        """
        logger = logging.getLogger(__name__)
        logger.setLevel(self._log_level)
        origin = request.headers.get('Origin')
        if origin and not allowed_hosts:  # no whitelist
            logger.warning("No whitelist specified. %s is allowed access.", origin)
            return True
        if not origin:
            logger.debug("Request contained no Origin header")
            return False
        parsed = urlparse(origin)
        return (parsed.netloc in allowed_hosts)

    def options(self):
        """Allow preflight requests."""
        self.set_status(204)  # success with no content
        self.finish()

    def setup(self):
        """Perform common setup actions for handlers."""
        self._config = self.application.config  # access to settings
        self._log_level = self.application.log_level
        self._app_name = self._config.APP_NAME
        if getattr(self._config, 'ENVIRONMENT') == 'dev':
            # allow only requesting origin explicitly if on development environment
            # NOTE: Browsers reject Access-Control-Allow-Origin='*' if using 'withCredentials: true'
            self.set_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))
        else:
            # set ALLOWED_HOSTS in the env-specific settings file for CORS
            allowed_hosts = getattr(self._config, 'ALLOWED_HOSTS', [])
            if self._is_allowed_origin(self.request, allowed_hosts):
                self.set_header('Access-Control-Allow-Origin', self.request.headers['Origin'])

        # CORS
        allowed_headers = ('content-type', 'x-requested-with', 'user-agent',
                           'if-modified-since', 'cache-control')
        self.set_header('Access-Control-Allow-Methods', ', '.join([s for s in self.SUPPORTED_METHODS]))
        self.set_header('Access-Control-Allow-Headers', ', '.join(allowed_headers))

    def write_error(self, status_code, message=None, exc_info=None):
        """Write an error with appropriate status code and message."""
        logger = logging.getLogger(__name__)
        logger.setLevel(self._log_level)
        default_msg = 'API Exception'
        if exc_info is not None:
            logger.error(message or default_msg, exc_info=exc_info)
        else:
            logger.error(message or default_msg)
        self.set_status(status_code)
        self.write({'error': message})
        self.finish()


class BaseHandler(HandlerMixin, tornado.web.RequestHandler):
    """Parent for other request handlers."""

    def initialize(self):
        """Standard init method per request."""
        super().initialize()
        self.setup()


# decorator for logging entry and exit of request handler endpoints
def log_sandwich(endpoint):
    """Fire simple log messages before and after endpoints."""
    def wrapper(self, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.setLevel(self._log_level)
        method = endpoint.__name__.upper()

        # sandwich
        logger.info("%s:%s start by anonymous", self.__class__.__name__, method)
        result = endpoint(self, *args, **kwargs)
        logger.info("%s:%s finish by anonymous", self.__class__.__name__, method)

        return result
    return wrapper
