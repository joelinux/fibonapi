#!/usr/bin/env python
from setuptools import find_packages, setup
from os import path as op


def _read(filename):
    """Read a file and return its contents."""
    try:
        return open(op.join(op.abspath(op.dirname(__file__)), filename)).read()
    except IOError:
        return ''


req_path = op.join('adm', 'pip_requirements.txt')
install_requires = [ln for ln in _read(req_path).split('\n') if ln and not ln.startswith('#')]


setup(
    name='fibonAPI',
    version='1.0.0',
    url='https://bitbucket.org/joelinux/fibonAPI',
    description=_read('DESCRIPTION'),
    long_description=_read('README.md'),
    author='Joey Espinosa',
    author_email='jlouis.espinosa@gmail.com',
    license='',
    keywords=[],
    install_requires=install_requires,
    packages=find_packages(),
    py_modules=['fibonapi', 'fibonapi.handlers', 'fibonapi.settings', 'fibonapi.tests'],
    classifiers=[],
    scripts=[],
    use_2to3=True,
)
