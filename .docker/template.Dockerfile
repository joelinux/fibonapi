FROM ##BASE##
LABEL maintainer "Joey Espinosa <jlouis.espinosa@gmail.com>"

# setup
USER root
ENV PROJECT_HOME /opt/fibonapi
RUN mkdir -p ${PROJECT_HOME} && \
    apk update && \
    apk --upgrade add \
        python3 \
        python3-dev \
        py3-pip \
        alpine-sdk

# project setup
COPY adm ${PROJECT_HOME}/adm
COPY ##NAME## ${PROJECT_HOME}/##NAME##
RUN pip3 install -r ${PROJECT_HOME}/adm/pip_requirements.txt

WORKDIR ${PROJECT_HOME}
ENV PYTHONPATH ${PROJECT_HOME}

# run code
EXPOSE ##PORT##
ENTRYPOINT ["python3", "##NAME##/app.py", "--port=##PORT##"]
