PROJECT_NAME := fibonapi
DATE = `date +'%Y%m%d%H%M%S'`
DOCKER_ARGS ?= -d
FIB_ENV ?= dev
NOCACHE ?=

BASE_IMAGE ?= alpine\:latest

# project specific
PORT ?= 8080

ifndef BRANCH
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
endif

ifeq ($(BRANCH),master)
	TAG = latest
else
	TAG = $(subst /,-,$(BRANCH))
endif

.setnocache:
	$(eval NOCACHE = --no-cache)

base:
	@docker pull $(BASE_IMAGE)

build: base container clean
	@docker tag $(PROJECT_NAME) $(PROJECT_NAME)\:$(TAG)

clean:
	@rm -rf Dockerfile
	@rm -rf cover

container:
	@sed \
		-e"s:##BASE##:$(BASE_IMAGE):g" \
		-e"s:##NAME##:$(PROJECT_NAME):g" \
		-e"s:##PORT##:$(PORT):g" \
		.docker/template.Dockerfile > Dockerfile
	@docker build --tag=$(PROJECT_NAME) --no-cache=true --rm=true --force-rm=true $(NOCACHE) .

deploy:
	@docker push $(PROJECT_NAME)\:$(TAG)

pull:
	@docker pull $(PROJECT_NAME)\:$(TAG)

rebuild: .setnocache build

start: stop
	@docker run --name="$(PROJECT_NAME)" \
		-h "$(PROJECT_NAME)" \
		-p $(PORT):$(PORT) \
		-e FIB_ENV=$(FIB_ENV) \
		$(DOCKER_ARGS) $(PROJECT_NAME)\:$(TAG)

stop:
	@(docker ps -a | awk '$$NF == "$(PROJECT_NAME)" {exit 1}') || docker rm -f $(PROJECT_NAME)

test:
	@FIB_ENV=test nosetests --with-coverage --cover-erase --cover-package=$(PROJECT_NAME) --cover-html
