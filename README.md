# FibonAPI
Simple API for returning a desired sequence of [Fibonacci](https://en.wikipedia.org/wiki/Fibonacci_number) numbers.

## Installation
#### From source
1. Clone this repo
2. Install requirements (Python 3.4+)
```bash
. adm/update_requirements.sh
```

## Run
#### Build Docker container
```bash
make build
make start
```
#### From source
```
python fibonapi/app.py --port=8080
```

## Test
#### Test the container (remove JQ portion if not installed)
```bash
$ curl http://localhost:8080/fibonacci/?n=10 | jq -r '.values[]'
0
1
1
2
3
5
8
13
```
#### Test the source
```bash
make test
```
