#!/bin/bash
# NOTE: This file runs a sub-script (and therefore, a shell).
#       You must run this script as ". <script>" or "source <script>".

CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
APT_CMD="$( which apt-get )"
BREW_CMD="$( which brew )"
PIP_FILE=${CURDIR}/pip_requirements.txt
APT_FILE=
BREW_FILE=
if [ ! -z "$APT_CMD" ]; then
    APT_FILE=${CURDIR}/apt_requirements.txt
else
    if [ ! -z "$BREW_CMD" ]; then
        BREW_FILE=${CURDIR}/brew_requirements.txt
    else
        echo "You need to either have apt-get installed or homebrew installed. Exiting."
    fi
fi
PREP_FILE=${CURDIR}/prep_requirements.txt
XTRA_FILE=${CURDIR}/extra_requirements.txt

# any prep commands that should run before apt
if [ -f ${PREP_FILE} ]; then
#    chmod +x ${PREP_FILE}
    echo "Executing preparation functions..."
    VIRTUALENVWRAPPER_PYTHON=${VIRTUALENVWRAPPER_PYTHON} $PREP_FILE
fi

# all OS packages (MUST come first!)
if [ ! -z "$APT_CMD" ] && [ -f ${APT_FILE} ]; then
    echo "Installing apt packages..."
    cat ${APT_FILE} | xargs sudo apt-get install -y
fi
if [ ! -z "$BREW_CMD" ] && [ -f ${BREW_FILE} ]; then
    echo "Installing brew packages..."
    cat ${BREW_FILE} | xargs brew install -y
fi

# all pip packages
if [ -f ${PIP_FILE} ]; then
    echo "Installing pip packages..."
    pip install -r ${PIP_FILE}
fi

# any final commands that should run
if [ -f ${XTRA_FILE} ]; then
#    chmod +x ${XTRA_FILE}
    echo "Installing extra requirements..."
    VIRTUALENVWRAPPER_PYTHON=${VIRTUALENVWRAPPER_PYTHON} $XTRA_FILE
fi

